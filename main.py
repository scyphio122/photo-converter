# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import argparse
import png_processor
import logging


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S')
    logging.root.setLevel(logging.NOTSET)

    argparser = argparse.ArgumentParser()
    argparser.add_argument("-i", "--image", help="path to the image",default="")
    argparser.add_argument("--bpp", help="Bits per pixel",default=-1)
    argparser.add_argument("--width", help="Converted image width in pixels",default="")
    argparser.add_argument("--height", help="Converted image heigth in pixels",default="")
    argparser.add_argument("-t", "--test", help="Performs test",action="store_true",default=False)
    argparser.add_argument("-b", "--bin", help="Exports binary image file",action="store_true",default=False)
    argparser.add_argument("-s", "--source", help="Exports C source file",action="store_true",default=False)
    argparser.add_argument("-r", "--rotate", help="Rotates the image: cw - clockwise, ccw - counterclokwise, exif - use EXIF data",default=-1)
    argparser.add_argument("--icon", help="Icon mode - no dithering, no image manipulation",action="store_true",default=False)

    argv = vars(argparser.parse_args())

    converter = png_processor.PngProcessor()

    if argv["image"] == "":
        logging.error("Image path not provided")
        exit(-1)

    if argv["bpp"] == -1:
        logging.error("BPP not set")
        exit(-1)

    if argv["icon"]:
        converter.set_icon_mode()

    if argv["test"]:
        converter.set_test()

    image_width, image_height = converter.open_jpeg(argv["image"])

    if (argv["width"] == "") or (argv["height"] == ""):
        logging.debug("Converted image not set, using input image size")
    else:
        image_width = argv["width"]
        image_height = argv["height"]

    converter.set_display_config(image_width, image_height, argv["bpp"])
    converter.rotate_image(argv["rotate"])
    converter.resize_image()
    converter.convert_image_to_grayscale()
    converter.change_palette_and_dither()

    if argv["test"]:
        converter.store_image_as_png()

    if argv["bin"]:
        image = converter.convert_to_eink_format()
        converter.store_binary_image_data(image)

    if argv["source"]:
        image = converter.convert_to_eink_format()
        converter.store_C_source_image_data(image)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
