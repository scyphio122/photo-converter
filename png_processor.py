from PIL import Image, ExifTags, ImageOps
import logging
import os
import math

# Depencency to pip3 Pillow package


class EOrientation(enumerate):
    E_ORIENTATION_PORTRAIT = 1,
    E_ORIENTATION_LANDSCAPE = 2


class PngProcessor:
    EINK_WIDTH = 800
    EINK_HEIGHT = 480
    EINK_BPP = 2
    
    def __init__(self):
        self.image = None
        self.jpeg_path = ""
        self.bpp = PngProcessor.EINK_BPP
        self.width = PngProcessor.EINK_WIDTH
        self.height = PngProcessor.EINK_HEIGHT
        self.test = False
        self.icon_mode = False
        self.orientation = EOrientation.E_ORIENTATION_PORTRAIT

    def set_test(self):
        self.test = True

    def set_icon_mode(self):
        self.icon_mode = True

    def set_display_config(self, width, height, bpp):
        self._set_bpp(int(bpp))
        self._set_display_resolution(int(width), int(height))
        if self.validate_input() != 0:
            logging.error("INVALID ARGUMENT INPUT")
            exit(-1)

    def validate_input(self):
        if self.bpp == 1:
            return 0
        else:
            if self.bpp == 2:
                if (self.width % 2) != 0:
                    logging.error("FOR 2BPP, WIDTH HAS TO BE MULTIPLE OF 2")
                    return -1
                else:
                    return 0
            else:
                if self.bpp == 4:
                    if (self.width % 4) != 0:
                        logging.error("FOR 4BPP, WIDTH HAS TO BE MULTIPLE OF 4")
                        return -1
                    else:
                        return 0
                else:
                    if self.bpp == 8:
                        if (self.width % 8) != 0:
                            logging.error("FOR 8BPP, WIDTH HAS TO BE MULTIPLE OF 8")
                            return -1
                        else:
                            return 0
                    else:
                        logging.error("INVALID BPP VALUE")
                        return -1

    def open_jpeg(self, jpeg_path):
        logging.debug("Opening JPEG image: " + jpeg_path)
        self.jpeg_path = jpeg_path
        self.image = Image.open(jpeg_path)
        return self.image.size

    def convert_image_to_grayscale(self):
        logging.debug("Converting image to grayscale")
        self.image = self.image.convert(
                        "L"
                    )
        return self.image

    def rotate_image(self, rotation):
        if rotation == "cw":
            self.image = self.image.rotate(90, expand=True)

        if rotation == "ccw":
            self.image = self.image.rotate(270, expand=True)

        if rotation == "exif":
            for orientation in ExifTags.TAGS.keys():
                if ExifTags.TAGS[orientation] == 'Orientation':
                    break

            exif = self.image._getexif()
            if exif != None:
                if exif[orientation] == 1:
                    self.orientation = EOrientation.E_ORIENTATION_LANDSCAPE

                if exif[orientation] == 3:
                    self.image = self.image.rotate(180, expand=True)
                    self.orientation = EOrientation.E_ORIENTATION_LANDSCAPE

                if exif[orientation] == 6:
                    self.orientation = EOrientation.E_ORIENTATION_PORTRAIT

                if exif[orientation] == 8:
                    self.image = self.image.rotate(180, expand=True)
                    self.orientation = EOrientation.E_ORIENTATION_PORTRAIT

                if self.orientation == EOrientation.E_ORIENTATION_LANDSCAPE:
                    logging.debug("Image orientation is LANDSCAPE")
                else:
                    logging.debug("Image orientation is PORTRAIT")

    def resize_image(self):
        logging.debug("Resizing image to: " + str(self.width) + "x" + str(self.height))
        self.image = self.image.resize((self.width, self.height), Image.LANCZOS)
        if self.icon_mode == False:
            self.image = ImageOps.autocontrast(self.image)
            self.image = ImageOps.equalize(self.image)

    def change_palette_and_dither(self):
        logging.debug("Changing palette and performing dithering")
        for y in range(0, self.height):
            for x in range(0, self.width):
                old_pixel = self.image.getpixel((x, y))
                new_pixel = self._get_closest_palette_colour(old_pixel)
                self.image.putpixel((x, y), new_pixel)

                if self.icon_mode == False:
                    quan = old_pixel - new_pixel  # Quantization error

                    if x != (self.width - 1):
                        value = (int)(quan * (float(7 / 16)) + self.image.getpixel((x + 1, y)))
                        self.image.putpixel((x + 1, y), value)

                    if (x != 0) and (y != self.height - 1):
                        value = (int)(quan * (float(3 / 16)) + self.image.getpixel((x - 1, y + 1)))
                        self.image.putpixel((x - 1, y + 1), value)

                    if y != self.height - 1:
                        value = (int)(quan * (float(5 / 16)) + self.image.getpixel((x, y + 1)))
                        self.image.putpixel((x, y + 1), value)

                    if (x != self.width - 1) and (y != self.height - 1):
                        value = (int)(quan * (float(1 / 16)) + self.image.getpixel((x + 1, y + 1)))
                        self.image.putpixel((x + 1, y + 1), value)

    def convert_to_eink_format(self):
        logging.debug("Converting image to EPD display format")
        #width, height = self.image.size
        out_threshold_val = int(256 / (2 ** self.bpp))

        pixels_per_byte = int(8 / self.bpp)
        new_image = bytearray((int)((self.width/pixels_per_byte) * self.height))

        for y in range(0, self.height):
            x = 0
            while x < self.width:
                if pixels_per_byte == 1:
                    pixel1_val = int( self.image.getpixel((x, y))  / out_threshold_val )
                    out_pixel = pixel1_val
                    x += pixels_per_byte
                else:
                    if pixels_per_byte == 2:
                        pixel1_val = int( self.image.getpixel((x, y)) / out_threshold_val )
                        pixel2_val = int( self.image.getpixel((x + 1, y)) / out_threshold_val )

                        out_pixel = (pixel1_val << 4) + (pixel2_val << 0)
                        x += pixels_per_byte
                    else:
                        if pixels_per_byte == 4:
                            pixel1_val = int( self.image.getpixel((x, y))  / out_threshold_val )
                            pixel2_val = int( self.image.getpixel((x + 1, y)) / out_threshold_val )
                            pixel3_val = int( self.image.getpixel((x + 2, y)) / out_threshold_val )
                            pixel4_val = int( self.image.getpixel((x + 3, y)) / out_threshold_val )

                            out_pixel = (pixel1_val << 6) + (pixel2_val << 4) + (pixel3_val << 2) + (pixel4_val << 0)
                            x += pixels_per_byte
                        else:
                            pixel1_val = int( self.image.getpixel((x, y)) / out_threshold_val )
                            pixel2_val = int( self.image.getpixel((x + 1, y)) / out_threshold_val )
                            pixel3_val = int( self.image.getpixel((x + 2, y)) / out_threshold_val )
                            pixel4_val = int( self.image.getpixel((x + 3, y)) / out_threshold_val )
                            pixel5_val = int( self.image.getpixel((x + 4, y)) / out_threshold_val )
                            pixel6_val = int( self.image.getpixel((x + 5, y)) / out_threshold_val )
                            pixel7_val = int( self.image.getpixel((x + 6, y)) / out_threshold_val )
                            pixel8_val = int( self.image.getpixel((x + 7, y)) / out_threshold_val )

                            out_pixel = (pixel1_val << 7) + (pixel2_val << 6) + (pixel3_val << 5) + (pixel4_val << 4) + \
                                        (pixel5_val << 3) + (pixel6_val << 2) + (pixel7_val << 1) + (pixel8_val << 0)
                            x += pixels_per_byte
            
                new_image[(y * int(self.width/pixels_per_byte)) + int((x/pixels_per_byte) - 1 )] = out_pixel
            
        return new_image

    def store_binary_image_data(self, data, output_directory = ""):
        logging.debug("Storing image as binary data")

        file_raw_name = os.path.basename(self.jpeg_path)
        file_raw_name = os.path.splitext(file_raw_name)[0]
        
        if output_directory == "":
            directory = os.path.dirname(self.jpeg_path)
            if not directory.endswith("/"):
                directory += "/"
            out_file = directory  + file_raw_name + ".bin"
        else:
            if not output_directory.endswith("/"):
                output_directory += "/"
            out_file = output_directory + file_raw_name + ".bin"
            
        image_file = open(out_file, 'wb')
        image_file.write(data)
        image_file.close()
        
        return out_file

    def store_Cpp_source_image_data(self, data):
        logging.debug("Storing image as C source file data")

        directory = os.path.dirname(self.jpeg_path)

        file_raw_name = os.path.basename(self.jpeg_path)
        file_raw_name = os.path.splitext(file_raw_name)[0]
        out_file = directory + "/" + file_raw_name + ".h"

        pixels_per_byte = int(8 / self.bpp)

        image_file = open(out_file, 'w')
        image_file.write("// " + file_raw_name + " image data\n")
        image_file.write("// Created with awsome Photo Converter APP\n")
        image_file.write("// For further support contact Rajesh a.k.a JSON\n\n\n")
        image_file.write("constexpr image<%d, %d, BPP_%d> " % (self.width, self.height, self.bpp) + file_raw_name + " PROGMEM")
        image_file.write(" = \n{{")
        
        for y in range(0, self.height):
            x = 0
            while x < self.width:
                image_file.write("0x%0.2X, " % data[(y * int(self.width/pixels_per_byte)) + int((x/pixels_per_byte) )])
                x += pixels_per_byte
            image_file.write('\n')

        image_file.write("}};\n")
        image_file.close()

    def store_C_source_image_data(self, data):
        logging.debug("Storing image as C source file data")

        directory = os.path.dirname(self.jpeg_path)

        file_raw_name = os.path.basename(self.jpeg_path)
        file_raw_name = os.path.splitext(file_raw_name)[0]
        out_file = directory + "/" + file_raw_name + ".h"

        pixels_per_byte = int(8 / self.bpp)

        image_file = open(out_file, 'w')
        image_file.write("/* " + file_raw_name + " image data */\n")
        image_file.write("/* Created with awsome Photo Converter APP */\n")
        image_file.write("/* For further support contact Rajesh a.k.a JSON */\n\n\n")
        image_file.write("const uint8_t " + file_raw_name + "_image_data[] = {")

        cnt = 0
        length = self.height * self.width
        for y in range(0, self.height):
            x = 0
            while x < self.width:
                if cnt % 12 == 0:
                    image_file.write("\n    ")
                image_file.write(
                    "0x%0.2X" % data[(y * int(self.width / pixels_per_byte)) + int((x / pixels_per_byte))])
                x += pixels_per_byte
                cnt += 1
                if cnt < length:
                    image_file.write(", ")

        image_file.write("\n};")
        image_file.close()

    def store_image_as_png(self):
        logging.debug("Storing converted image as PNG")
        directory = os.path.dirname(self.jpeg_path)
        file_raw_name = os.path.basename(self.jpeg_path)
        file_raw_name = os.path.splitext(file_raw_name)[0]
        out_file_path = directory + "/" + file_raw_name + "_out.png"
        self.image.save(out_file_path, "PNG")

        # @var test [in] - True if the image is to be shown on the host computer
    def _get_closest_palette_colour(self, pixel_val):
        out_threshold_val = int(256 / (2 ** self.bpp))
        out_threshold_cnt = 2 ** self.bpp
        for i in range(0, out_threshold_cnt - 1):
            if math.fabs(pixel_val - (i * out_threshold_val)) <= (out_threshold_val / 2):
                #if not self.test:
                #    return i
                return i * out_threshold_val

        #if not self.test:
        #    return out_threshold_cnt - 1
        #else:
        return (out_threshold_cnt * out_threshold_val) - 1

    def _set_bpp(self, bpp):
        if (bpp != 1) and (bpp != 2) and (bpp != 4) and (bpp != 8):
            logging.error("Invalid BPP value: " + bpp)
            exit(-1)
        else:
            self.bpp = bpp

    def _set_display_resolution(self, width, height):
        self.width = width
        self.height = height
