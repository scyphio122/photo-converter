# Photo converter

This python application is intended to perform image convertion to the format understandable to the EPD display controller.

## Description
 What the application does is:

* Reads jpeg/png images
* Converts RGBA image to Grayscale
* Rotates the image to the valid orientation, accordingly to the main direction (landscape or portrait) - this step prevents from image being upside down. Rotation is automatic or manual.
* Resize of the image to the final resolution
* Performs colour palette change and introduces dithering accordingly to the Floyd-Steinberd alghoritm. The colour palette depends on the EPD display bits per pixel (BPP)
* Stores the processed image as PNG (for DEBUG), binary or C/C++ header file.

   
## Usage

The application uses the CLI interface. The possible arguments are:

* -i/--image - Path to the image to be converted
* --bpp - Number of bits per pixel of the EPD display. Valid values are: 1, 2, 4 and 8
* --width - width in pixels of the EPD display, if ommited then image is not resized
* --height - height in pixels of the EPD display, if ommited then image is not resized
* -r/--rotate - the application will rotate the image, valid parameters are: cw - clockwise, ccw - counterclokwise, exif - use EXIF data
* -t/--test - the application will store the processed output as the PNG image. 
* -b/--bin - the application will store the processed output as binary file
* -s/--source - the application will store the processed output as C/C++ header file
* --icon - icon export mode, in this mode no dithering and no image manipulation (autocontrast and equalization) are performed

## Dependencies

The application to run properly needs:

* Python 3.8
* Pillow package: `pip3 install Pillow`
